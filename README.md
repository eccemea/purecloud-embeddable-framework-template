## What is this?

It's a template of Customer's CRM page. It contain prefilled places where you can put your code to make integration ready.

## Start

* Run localserver on HTTPS (443) & open page https://localhost/index.html

* **Windows** SSL certificate (go further below if you have a Mac)
    * Download and install [OpenSSL](http://gnuwin32.sourceforge.net/packages/openssl.htm)
    * Download a sample openssl.cnf from [here](https://www.tbs-certificates.co.uk/openssl-dem-server-cert-thvs.cnf). Save it in the `C:\Program Files (x86)\GnuWin32\bin` folder and rename it `openssl.cnf`
    * Open an **Administrator** Command Prompt window
    * Add a new environment variable called `OPENSSL_CONF` to set the default location for the OpenSSL configuration file
        * `set OPENSSL_CONF=C:\Program Files (x86)\GnuWin32\bin\openssl.cnf`
        * (optional) `set Path=%PATH%;C:Program Files (x86)\GnuWin32\bin` to run openssl from any location
    * Run `openssl genrsa -des3 -out server.key 1024` to generate a private key
    * Run `openssl req -new -key server.key -out server.csr` to generate a CSR (Certificate Signing Request) - Use all the default values
    * Run `openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt` to generate a self-signed certificate
    * Create a `.vscode` folder in your work folder in Visual Studio Code
    * Create a `settings.json` file in the `.vscode` folder
    * Paste the following code:
    
```json
    {
      "liveServer.settings.https": {
        "enable": true,
        "cert": "<PATH TO server.crt FILE>",
        "key": "<PATH TO server.key FILE>",
        "passphrase": "12345"
      }
    }
```
    
* Mac SSL certificate
    * TODO


* Start the `Live Server` by clicking on the bottom-right `Go Live` button
    


2. It should display basic frame for EF, without any connectivity to PureCloud
3. From resource center find and create basic window.Framework object (https://developer.mypurecloud.ie/api/embeddable-framework/) inside framework.js file.
4. Try to run Embeeded Framework without any customization first. Basic approach, with only Auth against your org. Please rember to create and fill in valid OAuth Client Id into your framework.js
*   HINT: To be able to view embeed window, please include EF iframe in index.html
5. Once you're able to display embeeded Framework, it's time to integrate it with CRM page. Integration is possible via window.addEventListener. You should be able to send event message from CRM to registered listener in framework.js and other way.
*   to send event from framework.js to CRM use 
`parent.postMessage(message, targetOrigin);`
*   to send event from CRM to framework.js (like Click to Dial) use below template 

`
var iframe = document.getElementById('clientIframe');  var message = {
    eventName: // custom event name,  number: // your number to be dialed  };
var targetOrigin = "https://apps.mypurecloud.ie";
iframe.contentWindow.postMessage(message, targetOrigin);`

* to receive events from other side, you need to register listener, use below template

`  window.addEventListener("message", function (event) {});`

## iFrame example
```
 <iframe allow="camera *; microphone * autoplay *" id="clientIframe"
                        src="https://apps.mypurecloud.ie/crm/index.html?crm=framework-local-secure" frameborder="0"
                        width="300" height="450">
                    </iframe> 
```


## Subscribe Hint
https://developer.mypurecloud.ie/api/embeddable-framework/actions/subscribe.html
